# SCSS Responsive Layout

This repository includes a responsive layout grid and breakpoints.

## Setup

##### Install

`npm install --save scss-responsive-layout`

##### Import

`@import "node_modules/scss-responsive-layout/layout"`

##### Variables

Overwrite the default variables. 

> Define own variables before default variables

```scss
$layout-max-container-width: 1440px !default;

$layout-grid-columns: 12 !default;

$layout-breakpoints: (
  xs: 0,
  sm: 768px,
  md: 1024px,
  lg: 1440px,
) !default;

$layout-grid-gap: (
  xs: 16px,
  sm: 24px,
  md: 32px,
  lg: 48px,
) !default;

$layout-grid-offset: (
  xs: 16px,
  sm: 32px,
  md: 48px,
  lg: 80px,
) !default;
```

## Usage

##### Layout
```html
<div class="l-container">
  <div class="l-row">
    <div class="l-col l-col-sm-6 l-col-md-4 l-col-lg-3">
      ...
    </div>
    <div class="l-col l-col-sm-6 l-col-md-4 l-col-lg-3">
      ...
    </div>
  </div>
</div>
```

##### Breakpoints

```scss
@include breakpoint(sm) {...} // @media only screen and (min-width: 768px) {...}
```
By adding `max` it will returns `max-width` instead of `min-width`
```scss
@include breakpoint(sm, max) {...} // @media only screen and (max-width: 767px) {...}
```
